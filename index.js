///input: nhận đầu vào là 3 số từ user
///math: tạo một string rỗng và so sánh theo từng số với nhau theo các trường hợp khác nhau
///sau đó dùng phương pháp cộng string vào string rỗng "result" theo thứ tự đã so sánh
///output: in ra màn hình string "result"       
function soTangDan(num1, num2, num3) {
    num1 = document.getElementById("num__1").value ;
    num2 = document.getElementById("num__2").value ;
    num3 = document.getElementById("num__3").value ;
    var result = ""
    if (num1 <= num2) {
        result = num1 + "," + num2.toString();
        document.getElementById("result__1").innerHTML = `Sắp xếp tăng dần: ${result}`;
        if (num2 <= num3) {
            result = result + "," + num3.toString();
            document.getElementById("result__1").innerHTML = `Sắp xếp tăng dần: ${result}`;
        } else if (num2 > num3 && num1 <= num3) {
            result = num1 + "," + num3.toString() + ","  + num2;
            document.getElementById("result__1").innerHTML = `Sắp xếp tăng dần: ${result}`;
        } else if (num1 > num3){
            result = num3 + "," + result;
            document.getElementById("result__1").innerHTML = `Sắp xếp tăng dần: ${result}`;
        }
    } else { ///n1 > n2
        result = num2 + "," + num1.toString();
        document.getElementById("result__1").innerHTML = `Sắp xếp tăng dần: ${result}`;
        if (num2 >= num3) {
            result = num3 + ","  +result;
            document.getElementById("result__1").innerHTML = `Sắp xếp tăng dần: ${result}`;
        } else if (num2 < num3 && num3 <= num1) {
            result = num2 + "," + num3.toString() + "," + num1;
            document.getElementById("result__1").innerHTML = `Sắp xếp tăng dần: ${result}`;
        } else if (num3 >= num1) {
            result = result + "," +num3;
            document.getElementById("result__1").innerHTML = `Sắp xếp tăng dần: ${result}`;
        }
    }
  
}

///input: tạo một dropdown menu chứa thông tin các thành viên trong gia đình
///math: sử dụng switch-case để in ra màn hình các kết quả chào hỏi ứng với dữ liệu mà user đã chọn
///output: in ra lời chào hỏi
function chaoHoi() {
    var member = document.getElementById("member");
    switch (member.value) {
        case "tv":
            document.getElementById("result__2").innerHTML = `Xin chào Người lạ !`;
            break;
        case "b":
            document.getElementById("result__2").innerHTML = `Xin chào Bố`;
            break;
        case "m":
            document.getElementById("result__2").innerHTML = `Xin chào Mẹ`;
            break;
        case "at":
            document.getElementById("result__2").innerHTML = `Xin chào Anh Trai`;
            break;
        case "eg":
            document.getElementById("result__2").innerHTML = `Xin chào Em Gái`;
            break;
        default:
            break;
    }
}

///input: nhận vào 3 số từ user
///math: kiểm tra dữ liệu nhập vào là dạng integer, nếu không sẽ báo không hợp lệ
///tạo ra 2 biến riêng biệt là: Số Chẵn và Số Lẻ
///kiểm tra xem 3 số user nhập vào là số chẵn hay lẻ bằng chắc chia lấy dư cho 2
///nếu là số chẵn thì biến Số Chẵn += 1 và ngược lại
///output: in ra 2 biến Số Chẵn và Số Lẻ
function soChanLe() {
    var num1 = document.getElementById("num__1-2").value * 1;
    var num2 = document.getElementById("num__2-2").value * 1;
    var num3 = document.getElementById("num__3-2").value * 1;
    if (Number.isInteger(num1) == false || Number.isInteger(num2) == false || Number.isInteger(num3) == false) {
        alert("Du lieu nhap vao khong hop le")
    }
    var soChan = 0;
    var soLe = 0;
    if (num1 % 2 == 0) {
        soChan += 1;
    } else { 
        soLe += 1;
    }
    if (num2 % 2 == 0) {
        soChan += 1;
    } else { 
        soLe += 1;
    }
    if (num3 % 2 == 0) {
        soChan += 1;
    } else { 
        soLe += 1;
    }
    document.getElementById("result__3").innerHTML = `Có ${soChan} số chẵn, Có ${soLe} số lẻ.`
}

///input: nhận dữ liệu 3 cạnh hình tam giác từ user
///math: kiểm tra dữ liệu hợp lệ, nếu không sẽ có thông báo
///so sánh 3 cạnh để xác định loại tam giác phù hợp
///output: in ra màn hình loại tam giác phù hợp với dữ liệu 3 cạnh
function hinhTamGiac() {
    var a = document.getElementById("canh__1").value * 1;
    var b = document.getElementById("canh__2").value * 1;
    var c = document.getElementById("canh__3").value * 1;
    
    if (a+b <= c || a+c <= b || b+c <= a) {
        alert("Dữ liệu không hợp lệ");
    } else if ( a == b && a == c ) {
        document.getElementById("result__4").innerHTML = `Đây là tam giác đều`;
    } else if (a == b || a == c || b == c) {
        document.getElementById("result__4").innerHTML = `Đây là tam giác cân`;
    } else if (a**2 + b**2 == c**2 || a**2 + c**2 == b**2 || b**2 + c**2 == a**2){
        document.getElementById("result__4").innerHTML = `Đây là tam giác vuông`;
    } else {
        document.getElementById("result__4").innerHTML = `Một loại tam giác khác`
    } 
}


///input: nhận vào dữ liệu ngày-tháng-năm từ user
///math: kiểm tra dữ liệu hợp lệ, ngày sẽ trong khoảng 1-31, tháng trong khoảng 1-12
///tạo một mảng dữ liệu bao gồm các tháng có 31 ngày, các tháng còn lại sẽ có 30 ngày, trừ tháng 2
///kiểm tra năm xem có phải năm nhuận không bằng công thức chia hết cho 4 nhưng không chia hết hoặc chia hết cho 400
///nếu năm nhuận tháng 2 sẽ có 29 ngày, nếu không sẽ có 28 ngày
///tạo ra 2 function riêng biệt để tính ngày trước và sau
///output: in ra màn hình ngày trước và sau dựa theo dữ liệu user nhập
function demNgayTruoc() {
    var day = document.getElementById("day").value * 1;
    var month = document.getElementById("month").value * 1;
    var year = document.getElementById("year").value * 1;
    if (Number.isInteger(day) == false || Number.isInteger(month) ==false || Number.isInteger(year) == false){
        alert("Du lieu khong hop le");
    }
    if (day < 1 || day >31){
        alert("Du lieu khong hop le");
    }
    if (month < 1 || month > 12){
        alert("Du lieu khong hop le");
    }
    const thangDu = {5:'5',7:'7',10:'10',12:'12'}
    
    if (month == 1){
        if (day == 1){
        document.getElementById("result__5").innerHTML = `31/12/${year-1}`;
        }else if (day> 1 && day <= 31){
            document.getElementById("result__5").innerHTML = `${day-1}/${month}/${year}`;
        }
    }else if (month == 3){
        if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)){ 
            if(day > 29){
                alert("Du lieu khong hop le")
            }else if(day == 1){
                document.getElementById("result__5").innerHTML = `29/${month-1}/${year}`;
            }else if (day> 1 && day <= 29){
                document.getElementById("result__5").innerHTML = `${day-1}/${month}/${year}`;
            }
        }else{
            if(day>28){
                alert("Du lieu khong hop le");
            }else if(day ==1){
                document.getElementById("result__5").innerHTML = `28/${month-1}/${year}`;    
            }else if (day> 1 && day <= 28){
                document.getElementById("result__5").innerHTML = `${day-1}/${month}/${year}`;
            }
        }
    }else if (month in thangDu == true){
        if(day == 1){
            document.getElementById("result__5").innerHTML = `30/${month-1}/${year}`;
        }else if (day> 1 && day <= 31){
            document.getElementById("result__5").innerHTML = `${day-1}/${month}/${year}`;
        }
        }else if (month in thangDu == false){
            if(day == 1){
                document.getElementById("result__5").innerHTML = `31/${month-1}/${year}`;
        }else if (day> 1 && day <= 30){
            document.getElementById("result__5").innerHTML = `${day-1}/${month}/${year}`;
        }
    }
}   

///tương tự như function bên trên
function demNgaySau() {
    var day = document.getElementById("day").value * 1;
    var month = document.getElementById("month").value * 1;
    var year = document.getElementById("year").value * 1;
    if (Number.isInteger(day) == false || Number.isInteger(month) ==false || Number.isInteger(year) == false){
        alert("Du lieu khong hop le");
    }
    if (day < 1 || day >31){
        alert("Du lieu khong hop le");
    }
    if (month < 1 || month > 12){
        alert("Du lieu khong hop le");
    }
    const thangDu = {5:'5',7:'7',10:'10',12:'12'}
    
    if (month == 12){
        if (day == 31){
        document.getElementById("result__6").innerHTML = `1/1/${year+1}`;
        }else if (day >= 1 && day < 31){
            document.getElementById("result__6").innerHTML = `${day+1}/${month}/${year}`;
        }
    }else if (month == 2){
        if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)){ 
            if(day > 29){
                alert("Du lieu khong hop le")
            }else if(day == 29){
                document.getElementById("result__6").innerHTML = `1/${month+1}/${year}`;
            }else if (day >= 1 && day < 29){
                document.getElementById("result__6").innerHTML = `${day+1}/${month}/${year}`;
            }
        }else{
            if(day>28){
                alert("Du lieu khong hop le");
            }else if(day ==28){
                document.getElementById("result__6").innerHTML = `28/${month+1}/${year}`;    
            }else if (day >= 1 && day < 28){
                document.getElementById("result__6").innerHTML = `${day+1}/${month}/${year}`;
            }
        }
    }else if (month in thangDu == true){
        if(day == 31){
            document.getElementById("result__6").innerHTML = `1/${month+1}/${year}`;
        }else if (day >= 1 && day < 31){
            document.getElementById("result__6").innerHTML = `${day+1}/${month}/${year}`;
        }
        }else if (month in thangDu == false){
            if(day == 30){
                document.getElementById("result__6").innerHTML = `1/${month+1}/${year}`;
        }else if (day >= 1 && day < 30){
            document.getElementById("result__6").innerHTML = `${day+1}/${month}/${year}`;
        }
    }
}   


///input: Nhận vào dữ liệu tháng-năm từ user
///math: kiểm tra dữ liệu hợp lệ
///kiểm tra năm nhuận hoặc không
///kiểm tra tháng đủ(31 ngày) hoặc tháng thiếu(30- ngày)
///output: in ra màn hình số ngày trong tháng-năm đó
function demNgayThang() {
    var month = document.getElementById("thang").value * 1;
    var year = document.getElementById("nam").value *1;
    if (month < 1 || month > 12 || Number.isInteger(month)==false){
        alert("Du lieu khong hop le");
    }
    if (year < 0 || Number.isInteger(year)==false){
        alert("Du lieu khong hop le");
    }
    const thangDu = {1:'1', 3:'3', 5:'5', 7:'7',8:'8', 10:'10',12:'12'};
    if (month == 2){
        if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)){
            document.getElementById("result__7").innerHTML = `Tháng ${month} năm ${year} có 29 ngày`;
        }else{
            document.getElementById("result__7").innerHTML = `Tháng ${month} năm ${year} có 28 ngày`;
        }
    }else if (month in thangDu){
        document.getElementById("result__7").innerHTML = `Tháng ${month} năm ${year} có 31 ngày`;

    }else{
        document.getElementById("result__7").innerHTML = `Tháng ${month} năm ${year} có 30 ngày`;
    }
}

///input: nhận vào số có 3 chữ số từ user
///math: kiểm tra dữ liệu hợp lệ là: số có 3 chữ số và không phải dạng float
///tạo một function để chuyển dạng số sang dạng chữ bằng cách sử dụng switch-case
///tìm số hàng đơn vị, số hàng chục, số hàng trăm bằng các biện pháp nghiệp vụ
///output: in ra màn hình cách đọc số bằng chữ
function docSo() {
    var input_number = document.getElementById("3digitNum").value * 1;
    if(input_number < 100 || input_number > 999 || Number.isInteger(input_number) == false){
        alert("Du lieu khong hop le")
    }
    function chuyenDoiSo(input){
        switch(input){
            case 1:
                return "một";
                break;
            case 2:
                return "hai";
                break;
            case 3:
                return "ba";
                break;
            case 4:
                return "bốn";
                break;
            case 5:
                return "năm";
                break;
            case 6:
                return "sáu";
                break;
            case 7:
                return "bảy";
                break;
            case 8:
                return "tám";
                break;
            case 9:
                return "chín";    
                break;
        }    
    }
    soDonVi = input_number % 10;
    soHangChuc = ((input_number - soDonVi) / 10) % 10;
    soHangTram = Math.floor(input_number/100)
    
    if (soDonVi == 0 && soHangChuc == 1){
        document.getElementById("result__8").innerHTML = `${chuyenDoiSo(soHangTram)} trăm mười`;
    }else if(soDonVi == 0 && soHangChuc > 1){
        document.getElementById("result__8").innerHTML = `${chuyenDoiSo(soHangTram)} trăm ${chuyenDoiSo(soHangChuc)} mươi`;
    }else if(soHangChuc == 1 && soDonVi != 0){
        document.getElementById("result__8").innerHTML = `${chuyenDoiSo(soHangTram)} trăm mười ${chuyenDoiSo(soDonVi)}`;
    }else if(soHangChuc == 0 && soDonVi ==0){
        document.getElementById("result__8").innerHTML = `${chuyenDoiSo(soHangTram)} trăm`;
    }else if(soHangChuc == 0 && soDonVi !=0){
        document.getElementById("result__8").innerHTML = `${chuyenDoiSo(soHangTram)} trăm lẻ ${chuyenDoiSo(soDonVi)}`;
    }else{
        document.getElementById("result__8").innerHTML = `${chuyenDoiSo(soHangTram)} trăm ${chuyenDoiSo(soHangChuc)} mươi ${chuyenDoiSo(soDonVi)}`;
    }
}